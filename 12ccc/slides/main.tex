\documentclass[mathserif,fleqn]{beamer}

\input{packages}
\input{macros}
\input{beamer}

\title{ 
 Teoría axiomática de conjuntos à la Dijkstra y Scholten
}

\author{\textbf{Ernesto Acosta} \and Bernarda Aldana \and Jaime
Bohórquez \and Camilo Rocha}


\institute[ECI \& PUJ]{
  \large Escuela Colombiana de Ingeniería \\
  Pontificia Universidad Javeriana
}

\date[CCC 09.20.2017]{
  12mo Congreso Colombiano de Computación\\
  Cali, Septiembre 20, 2017
}

% PDF settings
\hypersetup{%
  pdftitle=\@title,%
  pdfauthor=\@author%
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Contexto}

  \begin{outeritemize}

  \item Históricamente, la lógica ha sido un objeto de estudio.

  \item Desde las ciencias de la computación hay un esfuerzo
    significativo para que la lógica sea una herramienta mecanizable.

  \item Recientemente se han propuesto cálculos lógicos que no
    solamente son útiles para la mecanización sino también para la
    enseñanza.

    \begin{inneritemize}
      \item[e.g.,] La lógica de E. W. Dijkstra y C. S. Scholten.
    \end{inneritemize}

  \end{outeritemize}
\end{frame}

\begin{frame}
  \frametitle{El problema}
  
  \begin{outeritemize}
  \item ¿Qué tanto esfuerzo toma reescribir la teoría axiomática de
    conjuntos de Zermelo y Frankel usando el sistema lógico de primer
    orden de Dijkstra y Scholten?
    
  \item ¿Qué tan útil es este resultado para el aprendizaje de la
    teoría de conjuntos por parte de estudiantes de pregrado en
    matemáticas?
  
  \end{outeritemize}
\end{frame}

\begin{frame}
  \frametitle{Contribución principal}

  \begin{outeritemize}
  
  \item Una axiomatización de la teoría de conjuntos de Zermelo y
    Frankel usando el sistema lógico de primer orden de Dijkstra y
    Scholten y su estilo calculatorio
    \begin{inneritemize}
      \item Se propone el sistema formal \alert{Set}.
    \end{inneritemize}
    
  \item Uso de \alert{Set} en un curso de pregrado en matemáticas de
    teoría axiomática de conjuntos durante dos semestres.
        
  \end{outeritemize}
\end{frame}

\section{The Formal System of Dijkstra and Scholten}



\begin{frame}
  \frametitle{El sistema formal de Dijkstra y Scholten}
\begin{definition}[Términos y fórmulas]\label{def.ds.form}
	
	Los términos y las fórmulas de $\DSL$ se construyen de la siguiente
  manera:
  
	{\footnotesize\begin{align*}
	t \; & {::=} \; x \mid c \mid f(t,\ldots,t) \\
	\varphi \; & {::=} \; \STRUE \mid \SFALSE  \mid t = t \mid P \mid Q(t,\ldots,t)
	\mid \NEG{\varphi} \mid \IFF{\varphi}{\varphi} \mid \XOR{\varphi}{\varphi} \mid
	\OR{\varphi}{\varphi} \\ &  \quad\, \mid \AND{\varphi}{\varphi} \mid \IMP{\varphi}{\varphi}
	\mid \CON{\varphi}{\varphi} \mid \ALL{x}{\varphi} \mid \EX{x}{\varphi}.
	  \end{align*}
  }

\end{definition}

\end{frame}

\begin{frame}
  \frametitle{El sistema formal de  Dijkstra y Scholten}
\begin{definition}[Axiomas]\label{def.fol.axioms}
  La {\em colección
		de axiomas de $\DSL$} está dada por el siguiente esquema axiomático:
	%
	\footnotesize{\begin{description}
		\item[$(Ax1)$] $\IFF{\IFF{\varphi}{\IFF{\psi}{\tau}}}{\IFF{\IFF{\varphi}{\psi}}{\tau}}$.
		\item[$(Ax2)$] $\IFF{\IFF{\varphi}{\psi}}{\IFF{\psi}{\varphi}}$.
		\item[$(Ax3)$] $\IFF{\IFF{\varphi}{\TRUE}}{\varphi}$.
		\item[$(Ax4)$] $\IFF{\OR{\varphi}{\OR{\psi}{\tau}}}{\OR{\OR{\varphi}{\psi}}{\tau}}$.
		\item[$(Ax5)$] $\IFF{\OR{\varphi}{\psi}}{\OR{\psi}{\varphi}}$.
		\item[$(Ax6)$] $\IFF{\OR{\varphi}{\FALSE}}{\varphi}$.
		\item[$(Ax7)$] $\IFF{\OR{\varphi}{\varphi}}{\varphi}$.
		\item[$(Ax8)$] $\IFF{\OR{\varphi}{\IFF{\psi}{\tau}}}{\IFF{\OR{\varphi}{\psi}}{\OR{\varphi}{\tau}}}$.
		\item[$(Ax9)$] $\IFF{\NEG{\varphi}}{\IFF{\varphi}{\FALSE}}$.
		\item[$(Ax10)$] $\IFF{\XOR{\varphi}{\psi}}{\IFF{\NEG{\varphi}}{\psi}}$.
	\end{description}}
	%
\end{definition}

\end{frame}

\begin{frame}
		\frametitle{El sistema formal de Dijkstra y Scholten}
		\begin{definition}[Axiomas]\label{def.fol.axioms}
			\footnotesize{\begin{description}
		\item[$(Ax11)$] $\IFF{\AND{\varphi}{\psi}}{\IFF{\varphi}{\IFF{\psi}{\OR{\varphi}{\psi}}}}.$
		\item[$(Ax12)$] $\IFF{\IMP{\varphi}{\psi}}{\IFF{\OR{\varphi}{\psi}}{\psi}}$.
		\item[$(Ax13)$] $\IFF{\CON{\varphi}{\psi}}{\IMP{\psi}{\varphi}}$.
		\item[$(Bx1)$] $\IFF{\ALL{x}{\varphi}}{\varphi},\;$ si $x$ no está libre en  $\varphi$.
		\item[$(Bx2)$]
		$\IFF{\OR{\varphi}{\ALL{x}{\psi}}}{\ALL{x}{\OR{\varphi}{\psi}}},\;$
		si $x$ no está libre en  $\varphi$.
		\item[$(Bx3)$]
		$\IFF{\AND{\ALL{x}{\varphi}}{\ALL{x}{\psi}}}{\ALL{x}{\AND{\varphi}{\psi}}}.$
		\item[$(Bx4)$] $\IMP{\ALL{x}{\varphi}}{\tsub{\varphi}{x}{t}},\;$ si $t$
		está libre para  $x$ en $\varphi$.
		\item[$(Bx5)$] $\IFF{\EX{x}{\varphi}}{\NEG{\ALL{x}{\NEG{\varphi}}}}$.
		\item[$(Bx6)$] $(x=x)$.
		\item[$(Bx7)$] $\IMP{(x=t)}{\IFF{\varphi}{\tsub{\varphi}{x}{t}}},\;$ si
		$t$ está libre para $x$ en $\varphi$.
	\end{description}}
	%
\end{definition}
\end{frame}

\begin{frame}
	\frametitle{El sistema formal de Dijkstra y Scholten}
	\begin{definition}\label{def.fol.rules}
    Las {\em reglas de inferencia} de $\DSL$ son:
		\begin{align*}
	    \qquad	\inferrule* [Right=Ecuanimidad] {\psi \\ \IFF{\psi}{\varphi}} {\varphi}
		\end{align*}
		\begin{align*}
	\qquad	\inferrule* [Right=Leibniz] {\IFF{\psi}{\tau}} 
		{\IFF{\tsub{\varphi}{P}{\psi}}{\tsub{\varphi}{P}{\tau}}} 
		\end{align*}
		\begin{align*}
	\qquad	\inferrule* [Right=Generalización.] {\varphi}{\ALL{x}{\varphi}} 
	\end{align*}
\end{definition}
\end{frame}

\section{Una teoría axiomática de conjuntos}

\begin{frame}
  \frametitle{Una teoría axiomática de conjuntos}
  \begin{definition}[Los axiomas de \alert{Set}]
    $\varphi,\psi\in\Fforms{\vars}{\funcs}{\preds}$,  $\varphi$ tiene exactamente una variable libre y  $\psi$ es univalente.\\ [0.3cm] 
    \footnotesize{
    $(Cx1)$\quad$\QALLS{x}{\AIFF{x = \MTS}{\QALLS{y}{\ANEG{y \in x}}}}$.\\ [0.2cm]
    $(Cx2)$\quad$\QALLS{x,y}{\AIFF{x = y}{\QALLS{u}{\AIFF{u \in x}{u \in y}}}}$.\\ [0.2cm]
    $(Cx3)$\quad$\QALLS{x,y,z}{\AIFF{x = \{y,z\}}{\QALLS{u}{\IFF{u \in
            x}{\AOR{u=y}{u=z}}}}}$.\\ [0.2cm]
    $(Cx4)$\quad$\QALLS{x,y}{\AIFF{y = \{u \in x \mid
        \varphi(u)\}}{\QALLS{u}{\AIFF{u \in y}{\AAND{u\in
              x}{\varphi(u)}}}}}$,\\ [0.2cm]
    $(Cx5)$\quad$\QALLS{x,y}{\AIFF{y=\bigcup x}{\QALLS{u}{\AIFF{u \in
            y}{\QEX{z}{z \in x}{u \in z}}}}}$.\\ [0.2cm]
    $(Cx6)$\quad$\QALLS{x,y}{\AIFF{y=\POWER x}{\QALLS{u}{\AIFF{u \in
            y}{\QALL{z}{z\in u}{z \in x}}}}}$.\\ [0.2cm]
    $(Cx7)$\quad$\QALLS{x,y}{\AIFF{y = \psi[x]}{\QALLS{u}{\AIFF{u\in
            y}{\QEX{z}{z \in x}{\psi(u,z)}}}}}$.\\ [0.2cm]
    $(Cx8)$\quad$\QEX{x}{\MTS \in x}{\QALL{y}{y \in x}{y \cup \{y\}
        \in x}}$.}
  \end{definition}
\end{frame}

\section{Demostraciones calculativas para el aula de clase}

\begin{frame}
  \frametitle{Una demostración calculativa}
  \begin{theorem}
    No hay un conjunto universal.
  \end{theorem}
  \begin{proof} Suponga que tal conjunto $V$ existe. Así,
  $\vdash_{\SETT} \QALLS{x}{x \in V}$. Considere el conjunto $S = \SET{x
    \in V}{\ANEG{x\in x}}$. Entonces 
  {\tiny
  	\begin{align*}
    \expr{S \in S}
    \expl{\SIFF}{definición de $S$}
    \expr{S \in \SET{x \in  V}{x \notin x}}
    \expl{\SIFF}{ axioma de separación $(Cx4)$ } 
    \expr{S \in V \land  S \notin S}
    \expl{\SIFF}{$V$ es el conjunto universal} 
    \expr{\AAND{\STRUE}{S \notin S}} 
    \expl{\SIFF}{ lógica proposicional} 
    \exprnnl{S \notin S.}
    \end{align*}
    }
  $\vdash_\SETT \AIFF{S \in S}{S \notin S}$ es una
  contradicción, y por lo tanto $V$ no puede existir.
\end{proof}
\end{frame}

\begin{frame}
  \frametitle{La exploración algebraica}

  \alert{Set} facilita el cálculo entre elementos del dominio de
  discurso.
\end{frame}


\begin{frame}
  \frametitle{La exploración algebraica}
\begin{example}
  La meta es simplificar $\bigcup \{\MTS, \{\MTS\}\}$:
\pause
  {\tiny
    \begin{align*}
      \expr{x\in \cup \{\MTS, \{\MTS\}\}}
      \expl{\equiv}{axiom of union $(Cx5)$}
      \expr{\QEX{y}{y \in \{\MTS, \{\MTS\}\}}{ x\in y}}
      \expl{\equiv}{axiom of pair $(Cx3)$}
      \expr{\QEX{y}{\AOR{y=\MTS}{y=\{\MTS\}}}{ x\in y}}
      \expl{\equiv}{azucar sintáctica para la cuantificación existencial; lógica proposicional}
      \expr{\QEXS{y}{\AOR{\AND{y =\MTS}{x\in y}}{\AND{y = \{\MTS\}}{ x\in y}}}}
      \expl{\equiv}{axioma del conjunto vacío $(Cx1)$: ningún elemento pertenece al conjunto vacío}
      \expr{\QEXS{y}{\AOR{\SFALSE}{\AND{y = \{\MTS\}}{ x\in y}}}}
      \expl{\equiv}{azucar sintáctica para la cuantificación existencial; lógica proposicional}
      \expr{\QEX{y}{y=\{\MTS\}}{x \in y}}
      \expl{\equiv}{exactamente un elemento satisface el rango}
      \exprnnl{x\in \{\MTS\}.}
    \end{align*}
  }
  %
Así, $\bigcup \{\MTS, \{\MTS\}\}=\{\MTS\}$.
\end{example}
\end{frame}

\begin{frame}
\frametitle{La exploración algebraica}
 \alert{Set} facilita la composición de funciones. \\ [0.3cm]
 Si $f$ y $g$ son funciones
 \[\QALLS{x,y}{\AIFF{(x,y)\in g\circ f}{\QEXS{z}{\AAND{(x,z)\in f}{(z,y) \in g}}}}.\]

$\left\langle f_i\mid i\in I \right\rangle$ denota la función $f$ con dominio $I$. \\ [0.3cm] 

Por ejemplo, la función $f(x)=x^2$con dominio $[0,1]$ puede representarse
como $\left\langle x^2\mid x\in [0,1] \right\rangle$.\\ [0.3cm] 
 
Así, $(u,v) \in \left\langle x^2\mid x\in [0,1] \right\rangle$
 significa que $v=u^2$ and that $u\in [0,1]$. \\ [0.3cm]
\end{frame}

\begin{frame}
  \frametitle{La exploración algebraica}

 \begin{example}
   La meta es calcular $\left\langle \sqrt{x}\mid x>0 \right\rangle\circ
   \left\langle x^2-1\mid x\in \mathbb R \right\rangle$:
 {\tiny
 \begin{align*}
 \expr{(u,v)\in \left\langle \sqrt{x}\mid x>0 \right\rangle\circ \left\langle 
 x^2-1\mid x\in \mathbb R \right\rangle}
 \end{align*}
       \vspace{-1.2cm}
       \begin{align*}
 \expl{\equiv}{definition of function composition}
 \expr{\QEXS{z}{\AAND{(u,z)\in \left\langle x^2-1 \mid x\in \mathbb R 
       \right\rangle}{(z,v)\in \left\langle \sqrt{x}\mid x>0 \right\rangle}}}
       \end{align*}
             \vspace{-1.2cm}
             \begin{align*}
 \expl{\equiv}{azucar sintáctica ; $\left\langle \_ \mid \_\right\rangle$ 
 notation }
 \expr{\QEXS{z}{\AAND{z= u^2-1}{\AAND{u\in \mathbb R}{v= \sqrt{z}\wedge z>0}}}}
 \end{align*}
       \vspace{-1.2cm}
       \begin{align*}
 \expl{\equiv}{Axioma $(Bx3)$; Axioma $(Bx1)$}
 \expr{\AAND{u\in \mathbb R}{\QEXS{z}{\AAND{z= u^2-1}{\AAND{v= \sqrt{z}}{z>0}}}}}
 \end{align*}
       \vspace{-1.2cm}
       \begin{align*}
 \expl{\equiv}{sólo un elemento satisface el rango}
 \expr{\AAND{u\in \mathbb R}{\AAND{v= \sqrt{u^2-1}}{u^2-1>0 }}}
   \end{align*}
         \vspace{-1.2cm}
         \begin{align*}
 \expl{\equiv}{azucar sintáctica; $\left\langle \_ \mid \_\right\rangle$ 
 notation }
 \exprnnl{(u,v)\in \left\langle \sqrt{x^2-1}\mid x^2-1>0 \right\rangle.}
 \end{align*}}
 %
 Por lo tanto, $\left\langle \sqrt{x}\mid x>0 \right\rangle\circ
 \left\langle x^2-1\mid x\in \mathbb R \right\rangle=\left\langle
 \sqrt{x^2-1}\mid x^2>1 \right\rangle$. 
 \end{example}
 
\end{frame}

\begin{frame}
  \frametitle{Descubrimiento de estructura lógica}  

  \begin{outeritemize}
    \item El estilo calculatorio requiere escribir las proposiciones en
      un lenguaje muy preciso, que al final, revela su estructura
      lógica.

    \item En una teoría axiomática, se espera que los argumentos en
      una demostración sean muy precisos, dejando de lado -- tanto como
      sea posible -- los coloquiales.
  \end{outeritemize}
\end{frame}

\begin{frame}
	\frametitle{Descubrimiento de estructura lógica}  

  {\small Sin mucha dificultad se puede demostrar, que el producto
    cartesiano de dos conjuntos es vacío si y solamente si uno de sus
    factores es vacío.}\pause
  
	{\tiny\begin{align*}
		\expr{x\times y=\MTS}
		\expl{\SIFF}{axioma del conjunto vacío $(Cx1)$}
		\expr{\QALLS{u,v}{\ANEG{(u,v)\in x\times y}}}
		\expl{\SIFF}{axioma del producto cartesiano $(Cx14)$}
		\expr{\QALLS{u,v}{\ANEG{\AND{u\in x}{v \in y}}}}
		\expl{\SIFF}{lógica proposicional: ley de De Morgan}
		\expr{\QALLS{u,v}{\AOR{u\notin x}{v \notin y}}}
		\expl{\SIFF}{lógica de primer orden}
		\expr{\AOR{\QALLS{u}{u \notin x}}{\QALLS{v}{v \notin y}}}
		\expl{\SIFF}{axioma del conjunto vacío $(Cx1)$}
		\exprnnl{\AOR{x=\MTS}{y=\MTS}.}
		\end{align*}}
\end{frame}

\section{Experiencia en el aula de clase}
\begin{frame}
  \frametitle{Experiencia en el aula de clase}
  \begin{outeritemize}
    \item Con el estilo calculatorio hay una oportunidad de leer de
      forma diferente los teoremas y repensar sus enunciados en la
      enseñanza de la matemática.

    \item Algunas veces:

      \begin{inneritemize}

      \item La complejidad de las proposiciones excede la capacidad de
        los estudiantes

      \item La complejidad del discurso informal opaca la simplicidad
        de la estructura lógica.

      \end{inneritemize}

    \item  Con el estilo Dijkstra-Scholten:

      \begin{inneritemize}

      \item Contamos con un lenguaje para explorar el significado de
        las proposiciones limpiándolas de las figuras linguísticas
        literarias.

      \item Al leer y escribir las demostraciones en este lenguaje se
        revela la estructura lógica escondida en los recovecos del
        lenguaje literario.

      \end{inneritemize}

  \end{outeritemize}
  
\end{frame}
  
\begin{frame}
\frametitle{¿Cuál ha sido la experiencia en el aula de clase con \alert{Set}?}

\begin{inneritemize}

\item Los estudiantes se familiarizan rápidamente con el sistema
  formal $\DSL$ antes de comenzar el estudio de la teoría de
  conjuntos.

\item Sin ser expertos en lógica y después de aprender estos
  rudimentos lógicos, desarrollan una habilidad para traducir los
  enunciados de los teoremas y sus demostraciones en un libro de texto
  como el de Hrbacek y Jech (1999).

\item A los estudiantes se les propone realizar el proceso inverso de
  leer y escribir en lenguaje literario lo que han escrito
  simbólicamente.

\item En etapas posteriores, los estudiantes comienzan a proponer sus
  propias demostraciones, diferentes a las encontradas en el libro de
  texto.

\end{inneritemize}
\end{frame}

\begin{frame}
\frametitle{¿Cuál ha sido la experiencia en el aula de clase con \alert{Set}?}

\begin{inneritemize}

\item Los estudiantes descubren la necesidad de introducir nuevos
  símbolos en el lenguaje para especificar lo que se dice de los
  objetos como parte del discurso simbólico.

\item Los profesores son muy rigurosos pidiendo a sus estudiantes
  escribir en lenguaje simbólico las proposiciones respetando su
  ortografía.

\item Finalmente, los estudiantes no tienen dificultad en aceptar la
  necesidad de introducir nuevos conjuntos a la teoría para
  `empaquetar' información para argumentar en un nivel más abstracto:
  conjunto-conjunto en lugar de conjunto-elemento.

\end{inneritemize}

\end{frame}

\section{Conclusiones}

%\begin{frame}
%	\frametitle{Agenda} 
%	\tableofcontents[currentsection]
%\end{frame}
\begin{frame}
  \frametitle{Conclusiones}
\begin{outeritemize}

\item Muchos libros de texto evitan tratar con la lógica formal
  directamente.
  \begin{inneritemize}
  \item Hay una creencia entre la comunidad de matemáticos que las
    demostraciones formales detalladas involucran una gran cantidad de
    detalles triviales que harían muy extensos los libros de texto:
    esto no sucede con \alert{Set}.
  \item La sustitución de iguales por iguales es la razón por la cual
    el enfoque calculatorio es práctico para el razonamiento humano.
  \end{inneritemize}

\item Con \alert{Set}, se favorece el uso de demostraciones formales
  al enseñar la teoría de conjuntos a nivel de pregrado.

\item Estos resultados son parte de un esfuerzo más grande de estudiar
  formalmente y mecanizar teorías en el enfoque algebraico de Dijkstra
  y Scholten.
  \begin{inneritemize}
  \item Estas áreas incluyen la topología, la teoría de números y la
    teoría de modelos finitos.
  \end{inneritemize}
\end{outeritemize}
\end{frame}

\begin{frame}
  \begin{center}
    {\large \alert{¿Preguntas?}}
  \end{center}

  \vspace{2cm}
  
  \begin{flushright}
    {\bf ¡Gracias!}
  \end{flushright}
\end{frame}

\end{document}
