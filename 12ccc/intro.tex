\section{Introduction}
\label{sec.intro}

Axiomatic set theory is the branch of mathematics that studies
collections of objects from the viewpoint of mathematical logic.  In
general, axiomatic set theory focuses on the properties of the
membership relation `${\in}$', given the existence of some basic sets
(e.g., the empty set).  Unlike `naive' set theory -- where definitions
are given in natural language, and Venn diagrams and Boolean algebra
are used to support reasoning about collections -- the axiomatic study
of sets begins with a set of axioms and then associates axiomatic
rules to suitably defined sets and constructive relations. Because
other theories across different branches of mathematics (e.g., number
theory, topology) can be encoded in set theory, it plays an important
role as foundational system.

An axiomatic theory for sets is usually given as a first-order logic
theory, i.e., as a formal system that uses: (i) universally and
existentially quantified variables over non-logical objects, and (ii)
formulas that can contain variables, function symbols, and predicate
symbols. Variables range over collections; function symbols include
the empty set, projections, and cardinality; and predicate symbols
include membership and equality. The Zermelo-Fraenkel (ZF) is the most
common axiomatic set theory~\cite{kunen-axsettheory-2013}, sometimes
including the axiom of choice (ZFC), which aims at formalizing the
notion of \textit{pure set} or \textit{hereditary well-founded set} so
that all entities in the universe of discourse are such collections.

This paper presents an axiomatization for set theory using the
\textit{calculational} approach developed by E. W. Dijkstra and
C. S. Scholten to formal logic~\cite{dijkstra-ds-1990}. Its main
contribution is a first-order theory for sets having as its key
feature the symbolic manipulation of formulas under the principle
known as Leibniz's rule: the substitution of `equals for
equals'. While there are many deductive systems for first-order logic,
both sound (i.e., all provable statements are true in all models) and
complete (i.e., all statements which are true in all models are
provable), the notion of proof in the Dijkstra-Scholten logic focuses
on logical equivalence rather than implication.  In general, the
Dijkstra-Scholten logic can be seen as a correct choice of
connectives, axioms, and inference rules, allowing for proofs of
logical formulas by symbol manipulation, without the need for
introducing unnecessary assumptions.

The algebraic approach by E. W. Dijkstra and C. S. Scholten to formal
logic, in general, is a \textit{proof
  calculus}~\cite{meseguer-genlogics-1989}. For the proposed axiomatic
theory, Dijkstra-Scholten refers to the logical system resulting from
the combination of first-order logic and their proof style.  The
notion of `proof' in the Dijkstra-Scholten system is actually a
\textit{derivation}~\cite{rocha-dsmeseguer-2015}, i.e., a sequence of
equivalences proved, mainly, by using Leibniz principle: if two
formulas are provably equivalent, then substituting one for the other
does not alter the meaning of any formula. In a nutshell, a derivation
can follow various approaches and can always be translated into a
formal proof~\cite{rocha-dsmeseguer-2015} (e.g., in a Hilbert-like
system). For an axiomatic theory of sets, derivations result in
rigorous and elegant counterparts to argumentative proofs, commonly
found in textbooks, which can help proving theorems succinctly.

The results presented in this paper are a partial report on a two-term
seminar experience to solve all exercises and symbolically rewrite all
proofs in sections 1, 2, and 9 of Chapter 1 in~\cite{jech-sets-1978},
using the Dijkstra-Scholten approach. The meta-mathematical aspects of
set theory such as the aspects of semantics, completeness, and axiom
independence are not considered since the main interest is to realize
how to teach formal thinking to undergraduates. A sophomore-year
course with this approach has been successfully taught twice.

This work is also a part of a larger effort to formally study and
mechanize topics in mathematics and computer science with the
algebraic approach of E. W. Dijkstra and C. S. Scholten. In
particular, the set theory axiomatization of ZF in first-order logic
presented in this paper is the first step towards a mechanization in
rewriting logic~\cite{meseguer-rltcs-1992}, a logic in which
concurrent rewriting coincides with logical deduction. What is
appealing about mechanizing theories à la Dijkstra-Scholten is that
they are written in a relatively strict format that can be easily
accessed by humans (which is seldom the case with most tools). In the
setting of rewriting logic, the notion of substitution of `equals for
equals' is a natural part of deduction because it is a more general
case of equational logic.

To sum up, the main contributions are:
\begin{itemize}
\item a set theory axiomatization of ZF in first-order logic using the
  calculational style of E. W. Dijkstra and C. S. Scholten;

\item examples of some proofs obtained by using derivations, compared
  to their argumentative versions found in textbooks; and

\item a discussion on how student experience has improved in a
  sophomore-year under\-graduate-level axiomatic set theory course
  taught à la Dijkstra and Scholten .

\end{itemize}

The rest of the paper is organized as follows. Section~\ref{sec.ds}
presents the first-order Dijkstra-Scholten
system. Section~\ref{sec.sets} presents the axiomatic set theory à la
Dijkstra-Scholten and Section~\ref{sec.proofs} presents examples of
proofs in this theory based on derivations. Section~\ref{sec.discuss}
presents a discussion explaining how the use of the proposed approach
has helped in teaching an undergraduate-level course on axiomatic set
theory. Finally, Section~\ref{sec.concl} presents some related work
and concluding remarks.
