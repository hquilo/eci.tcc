\section{The Formal System of Dijkstra and Scholten}
\label{sec.ds}

This section overviews the Dijkstra and Scholten first-order formal
system, summarizing sections 2-5 in~\cite{rocha-dsmeseguer-2015}.

A formal system uses an alphabet to construct a formal language from a
collection of axioms through inferential rules of formation. More
precisely, a formal system~\cite{hodel-intrologic-2013} consists of: a
(possibly infinite) collection of symbols or alphabet; a grammar
defining how well-formed formulas are constructed based on symbols in
the alphabet; a collection of axioms; and a collection of inference
rules. The collections of formulas of the formal systems presented
here, all have a decidable membership problem.

\begin{definition}\label{def.ds.proof}
Let $\fsF$ be a formal system, $\Gamma$ a collection of
$\fsF$-formulas, and $\varphi_n$ a $\fsF$-formula. A {\em proof of
  $\varphi_n$ from $\Gamma$ in $\fsF$} is a sequence of
$\fsF$-formulas $\varphi_0,\varphi_1,\ldots,\varphi_n$ such that for
any $0 \leq i \leq n$: (i) $\varphi_i$ is an axiom, (ii)
$\varphi_i\in\Gamma$, or (iii) $\varphi_i$ is the conclusion of an
inference rule with premises appearing in
$\varphi_0,\ldots,\varphi_{i-1}$. An $\fsF$-formula {\em $\varphi$ is
  a theorem from $\Gamma$ in $\fsF$}, denoted as $\Gamma \vdash_\fsF
\varphi$, if and only if there is a proof of $\varphi$ from $\Gamma$
in $\fsF$; in the case when $\Gamma = \emptyset$, $\varphi$ is called
a {\em theorem of $\fsF$}, denoted as $\vdash_\fsF \varphi$.
\end{definition}

The first-order system of E. W. Dijkstra and C. S. Scholten (with
equality) is presented as the formal system $\DSL$, which is
parametric on a first-order language $\mathcal{L}$.


\begin{definition}\label{def.ds.lang}
  The symbols of $\DSL$ are:
%
  \begin{itemize}
    \item An infinite collection $\vars$ of {\em variables}
      $x_0,x_1,x_2,\ldots$.
    \item A collection $\funcs$ of {\em function symbols}.
    \item A collection $\preds$ of {\em predicate symbols}, which
      includes infinitely many {\em constants} \\$P_0, P_1, P_2,
      \ldots$.
    \item An {\em arity function} $\ff{ar} : \funcs \cup \preds \to \nats$
      for function and predicate symbols.
    \item Left parenthesis `$($', right parenthesis `$)$', and comma `$,$'.
    \item The {\em logical connectives} $\STRUE, \SFALSE, \SNEG,\SIFF,
      \SXOR, \SOR, \SAND, \SIMP, \SCON, \SALL, \SEX$.
  \end{itemize}
%
\end{definition}

The infinitely many constant predicate symbols assumed to be in
$\preds$ are key for formula manipulation in the formal system. The
logical connectives of $\DSL$ include the Boolean constants $\STRUE$
and $\SFALSE$, negation `$\SNEG$', equivalence `$\SIFF$', discrepancy
`$\SXOR$', disjunction `$\SOR$', conjunction `$\SAND$', implication
`$\SIMP$', consequence `$\SCON$', `$\SALL$' for universal
quantification, and `$\SEX$' for existential quantification.

Terms and formulas in $\DSL$ are built in the usual way. A term is
built from variables and the application of a function symbol to a
sequence of terms. A formula is built from the Boolean constants, term
equality, and Boolean combination of formulas, with the application of
a predicate symbol to a sequence of terms and universal/existential
quantified formulas. The {\em atomic formulas} of $\DSL$ are the
Boolean constants $\TRUE$ and $\FALSE$, equality of terms, and the
formulas obtained by applying a predicate symbol to zero or more
terms. Definition~\ref{def.ds.form} introduces the terms and formulas
of $\DSL$.

\begin{definition}\label{def.ds.form}
  The collection of {\em terms} and the collection of {\em formulas}
  of the formal system $\DSL$ are given by the following BNF
  definitions, where $x \in \vars$, $c \in \funcs$ with
  $\ff{ar}(c)=0$, $f \in \funcs$ with $\ff{ar}=m>0$, $P\in \preds$
  with $\ff{ar}(P)=0$, $Q\in\preds$ with $\ff{ar}(Q)=n>0$, $t$ is a
  term, and $\varphi$ is a formula:
%
  \begin{align*}
    t \; & {::=} \; x \mid c \mid f(t,\ldots,t) \\
    \varphi \; & {::=} \; \STRUE \mid \SFALSE  \mid t = t \mid P \mid Q(t,\ldots,t)
    \mid \NEG{\varphi} \mid \IFF{\varphi}{\varphi} \mid \XOR{\varphi}{\varphi} \mid
    \OR{\varphi}{\varphi} \\ &  \quad\, \mid \AND{\varphi}{\varphi} \mid \IMP{\varphi}{\varphi}
    \mid \CON{\varphi}{\varphi} \mid \ALL{x}{\varphi} \mid \EX{x}{\varphi}.
  \end{align*}
%
  The expressions $\Fterms{\vars}{\funcs}$ and
  $\Fforms{\vars}{\funcs}{\preds}$ denote the collection of terms and
  the collection of formulas over $\vars$, $\funcs$, and $\preds$,
  respectively.
\end{definition}

In the Dijkstra-Scholten first-order logic, the textual substitution
operator $\_[\_:=\_]$ is overloaded both for replacing variables for
terms and for replacing constant predicate symbols for formulas. The
concept of a free occurrence of a variable in a formula in the
Dijkstra-Scholten logic is the traditional one, i.e., an occurrence of
a variable $x$ in a formula $\varphi$ is {\em free} iff such an
occurrence of $x$ is not under the scope of a $\SALL x$ or $\SEX
x$. Similarly, a term $t$ is {\em free} for $x$ in a formula $\varphi$
iff every free occurrence of $x$ in $\varphi$ is such that if it is under
the scope of a $\SALL y$ or $\SEX y$, then $y$ is not a variable in
$t$.

\begin{definition}\label{def.fol.axioms}
  Let $x \in \vars$, $t \in \Fterms{\vars}{\funcs}$, and
  $\varphi,\psi\in\Fforms{\vars}{\funcs}{\preds}$. The {\em collection
    of axioms of $\DSL$} is given by the following axiom schemata:
  %
  \begin{description}
  \item[$(Ax1)$] $\IFF{\IFF{\varphi}{\IFF{\psi}{\tau}}}{\IFF{\IFF{\varphi}{\psi}}{\tau}}$.
  \item[$(Ax2)$] $\IFF{\IFF{\varphi}{\psi}}{\IFF{\psi}{\varphi}}$.
  \item[$(Ax3)$] $\IFF{\IFF{\varphi}{\TRUE}}{\varphi}$.
  \item[$(Ax4)$] $\IFF{\OR{\varphi}{\OR{\psi}{\tau}}}{\OR{\OR{\varphi}{\psi}}{\tau}}$.
  \item[$(Ax5)$] $\IFF{\OR{\varphi}{\psi}}{\OR{\psi}{\varphi}}$.
  \item[$(Ax6)$] $\IFF{\OR{\varphi}{\FALSE}}{\varphi}$.
  \item[$(Ax7)$] $\IFF{\OR{\varphi}{\varphi}}{\varphi}$.
  \item[$(Ax8)$] $\IFF{\OR{\varphi}{\IFF{\psi}{\tau}}}{\IFF{\OR{\varphi}{\psi}}{\OR{\varphi}{\tau}}}$.
  \item[$(Ax9)$] $\IFF{\NEG{\varphi}}{\IFF{\varphi}{\FALSE}}$.
  \item[$(Ax10)$] $\IFF{\XOR{\varphi}{\psi}}{\IFF{\NEG{\varphi}}{\psi}}$.
  \item[$(Ax11)$] $\IFF{\AND{\varphi}{\psi}}{\IFF{\varphi}{\IFF{\psi}{\OR{\varphi}{\psi}}}}$.
  \item[$(Ax12)$] $\IFF{\IMP{\varphi}{\psi}}{\IFF{\OR{\varphi}{\psi}}{\psi}}$.
  \item[$(Ax13)$] $\IFF{\CON{\varphi}{\psi}}{\IMP{\psi}{\varphi}}$.
  \item[$(Bx1)$] $\IFF{\ALL{x}{\varphi}}{\varphi},\;$ if $x$ is not free
    in $\varphi$.
  \item[$(Bx2)$]
    $\IFF{\OR{\varphi}{\ALL{x}{\psi}}}{\ALL{x}{\OR{\varphi}{\psi}}},\;$
    if $x$ is not free in $\varphi$.
  \item[$(Bx3)$]
    $\IFF{\AND{\ALL{x}{\varphi}}{\ALL{x}{\psi}}}{\ALL{x}{\AND{\varphi}{\psi}}}.$
  \item[$(Bx4)$] $\IMP{\ALL{x}{\varphi}}{\tsub{\varphi}{x}{t}},\;$ if $t$
    is free for $x$ in $\varphi$.
  \item[$(Bx5)$] $\IFF{\EX{x}{\varphi}}{\NEG{\ALL{x}{\NEG{\varphi}}}}$.
  \item[$(Bx6)$] $(x=x)$.
  \item[$(Bx7)$] $\IMP{(x=t)}{\IFF{\varphi}{\tsub{\varphi}{x}{t}}},\;$ if
    $t$ is free for $x$ in $\varphi$.
  \end{description}
  %
\end{definition}
%
The axioms of $\DSL$ can be divided into two groups, namely,
$(Ax1)$-$(Ax13)$ and $(Bx1)$-$(Bx7)$. Axioms $(Ax1)$, $(Ax2)$, and
$(Ax3)$ define that equivalence is associative, commutative, and has
identity element $\STRUE$. Similarly, axioms $(Ax4)$, $(Ax5)$, and
$(Ax6)$ define that disjunction is associative, commutative, and has
identity element $\SFALSE$. Disjunction is idempotent by Axiom $(Ax7$)
and distributes over equivalence by Axiom $(Ax8)$. The remaining
axioms $(Ax9)$-$(Ax13)$ present axiomatic definitions for the
connectives in the propositional fragment of $\DSL$.  Axiom $(Bx1)$
states that a universal quantifier on variable $x$ can be omitted
whenever the formula it quantifies has no free occurrences of
$x$. Axiom $(Bx2)$ states that disjunction distributes over universal
quantification whenever there is no variable capture, while Axiom
$(Bx3)$ states that conjunction and universal quantification
commute. By Axiom $(Bx4)$, it is possible to particularize any
universal quantification with a term $t$ whenever the variables in $t$
are not captured by the substitution. Finally, Axiom $(Bx5)$ is an
axiomatic definition for existential quantification.

Note that by having $\{P_0,P_1,\ldots\} \subseteq \preds$ in
Definition~\ref{def.ds.lang}, propositions over propositional
variables $\{p_0,p_1,\ldots\}$ can be represented as atomic formulas
in $\Fforms{\MTS}{\MTS}{\preds}$ via the mapping $p_i \mapsto
P_i$. With this embedding, axioms $(Ax1)$-$(Ax13)$ characterize the
set $\{\STRUE,\SFALSE,\SIFF,\SOR\}$ as a complete collection of
connectives for the propositional fragment of $\DSL$. Likewise,
$\{\TRUE,\FALSE,\SIFF,\SOR,\SALL\}$ is a complete collection of
connectives for $\DSL$.

\begin{definition}\label{def.fol.rules}
  Let $x \in \vars$, $P\in\preds$ with $\ff{ar}(P)=0$, and
  $\varphi,\psi,\tau \in \Fforms{\vars}{\funcs}{\preds}$. The {\em
    inference rules} of $\DSL$ are:
  %
  {\small
    \begin{align*}
    \inferrule* [Right=Equanimity] {\psi \\ \IFF{\psi}{\varphi}} {\varphi}
    \qquad\qquad\qquad
    \inferrule* [Right=Leibniz] {\IFF{\psi}{\tau}} 
                {\IFF{\tsub{\varphi}{P}{\psi}}{\tsub{\varphi}{P}{\tau}}} 
    \qquad\qquad\quad \inferrule* [Right=Generalization.] {\varphi}{\ALL{x}{\varphi}} 
  \end{align*}}
  %
\end{definition}

Rules {\sc Equanimity} and {\sc Leibniz} allow for symbolic
manipulation based on equality by substitution of `equals for
equals'. Rule {\sc Generalization} is the usual first-order rule
stating that universally quantifying any theorem results in a theorem.
The assumption about the {\em infinite} collection of constant
predicate symbols in $\DSL$ is key for the Rule {\sc Leibniz} to work
when substituting formulas in any given formula.  Another important
fact regarding Rule {\sc Leibniz} is that from it some meta-properties
can be proved with almost no effort: (i) any substitution instance of
a tautology (i.e., of a theorem in the propositional fragment of
$\DSL$) is a theorem of $\DSL$ and (ii) the collection of theorems of
$\DSL$ is closed under formula substitution.

`Proofs' in the Dijkstra-Scholten calculational style are not strict
in the sense of a formal system. Instead, they are sequences of
formulas related, mainly, by equivalence. This approach takes
advantage of the transitive properties of the connectives to obtain
compact proof calculations.

\begin{definition}\label{sec.deriv.deriv}
  Let $\Gamma$ be a collection of formulas of $\DSL$.  A {\em
    derivation from $\Gamma$} in $\DSL$ is a non-empty finite sequence
  of formulas $\varphi_0, \varphi_1,\ldots,\varphi_n$ of $\DSL$
  satisfying, for any $0 < k \le n$, $\Gamma \vdash_\DSL
  \IFF{\varphi_{k-1}}{\varphi_k}$.
\end{definition}

The connection between a derivation and a proof is made precise in
Proposition~\ref{prop.ds.proof}.

\begin{proposition}{\cite{rocha-dsmeseguer-2015}}\label{prop.ds.proof}
  Let $\Gamma$ be a collection of formulas of $\DSL$ and
  $\varphi_0,\varphi_1,\ldots,\varphi_n$ be a derivation in $\DSL$
  from $\Gamma$.  It holds that $\Gamma\vdash_\DSL
  \IFF{\varphi_0}{\varphi_n}$.
\end{proposition}

It is important to note that any proof in the formal system $\DSL$ is
a derivation in $\DSL$ but a derivation is {\em not} necessarily a
proof. Consider, for instance, the sequence ``$\SFALSE,\SFALSE$''
which is a derivation because Boolean equivalence is reflexive, but
this sequence is not a proof because $\SFALSE$ is not a theorem. The
key fact about proofs in a formal system is that every formula in a
proof is a theorem, while this is not necessarily the case in a
derivation. There are other types of derivations where implication or
consequence can be combined with equivalence
(see~\cite{rocha-dsmeseguer-2015} for details).

In practice, derivations are not written directly as a sequence of
formulas but as a bi-dimensional arrangement of formulas and text
explaining each derivation step.

\begin{remark}
  A derivation $\varphi_0,\varphi_1,\ldots,\varphi_n$ from $\Gamma$ in
  $\DSL$ is usually written as:
%
  {\small\begin{align*}
    \expr{\varphi_0}
    \expl{\SIFF}{``explanation$_0$''}
    \expr{\varphi_1}
    \expl{\vdots}{\dots}
    \expr{\varphi_{n-1}}
    \expl{\SIFF}{``explanation$_{n-1}$''}
    \exprnnl{\varphi_n}
  \end{align*}}
%
  in which ``explanation$_i$'' is a text describing why
  $\Gamma\vdash_\DS\IFF{\varphi_i}{\varphi_{i+1}}$.
\end{remark}



Finally, the Dijkstra-Scholten logic proposes an alternative notation
for writing quantified formulas. The main idea is that proof
verification and derivation in such a syntax becomes simpler thanks to
the resemblance between, for example, the notation of a (finite)
quantification and the operational semantics of repetitive constructs
in an imperative programming language.

\begin{remark}\label{rem.ds.quant}
  Let $x\in\vars$ and $\varphi,\psi$ be formulas of $\DSL$.
  \begin{itemize}
    \item The expression $\QALL{x}{\psi}{\varphi}$ is syntactic sugar
      for $\ALL{x}{\IMP{\psi}{\varphi}}$; in particular,
      $\QALL{x}{\TRUE}{\varphi}$ can be written as $\QALLS{x}{\varphi}$.
    \item The expression $\QEX{x}{\psi}{\varphi}$ is syntactic sugar
      for $\EX{x}{\AND{\psi}{\varphi}}$; in particular,
      $\QEX{x}{\TRUE}{\varphi}$ can be written as $\QEXS{x}{\varphi}$.
  \end{itemize}
%
  In the formulas $\QALL{x}{\psi}{\varphi}$ and $\QEX{x}{\psi}{\varphi}$,
  $\psi$ is called the {\em range} and $\varphi$ the {\em subject} of the
  quantification.
\end{remark}

