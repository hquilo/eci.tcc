\section{Calculational Proofs for the Classroom}
\label{sec.proofs}

This section presents some notorious features of $\SETT$ that have
been identified, mainly, by teaching a 16-weeks undergraduate set
theory course. It is important to note that most of the students in
such a course are in their sophomore year and have had very little
exposure to mathematical logic. First, as it is often the case in set
theory, computation of operations between sets depends heavily on the
axiom of extensionality $(Cx2)$. Since $\SETT$ is based mainly on
Boolean equivalence, algebraic manipulations are simple to grasp and
can help a student in discovering proofs. Second, the precise language
required for writing formulas exposes their logical structure, thus
making it possible in a proof to transform one formula into another in
a clean way. Furthermore, in the Dijkstra-Scholten style a student can
deal symbolically with the parts of a proof argument that have to do
exclusively with propositional logic, usually hidden in a rhetorical
argument. Finally, $\SETT$ helps to identify the logical structure of
a theorem text and to anticipate relevant lemmas for its proof.

\subsection{Algebraic Exploration}

One of the advantages of $\SETT$ is that it facilitates the
computation of operations between elements in the domain of
discourse. The axiom of extensionality $(Cx2)$ is key in situations
when the goal is to transform a formula $x \in A$ to another formula
$x \in B$. In this setting, $A$ is a set whose definition is known,
while $B$ is a set to be found.

\begin{example}\label{exa.proofs.pairing}
  The goal is to simplify $\bigcup \{\MTS, \{\MTS\}\}$:
%
  \small{
    \begin{align*}
      \expr{x\in \cup \{\MTS, \{\MTS\}\}}
      \expl{\equiv}{axiom of union $(Cx5)$}
      \expr{\QEX{y}{y \in \{\MTS, \{\MTS\}\}}{ x\in y}}
      \expl{\equiv}{axiom of pair $(Cx3)$}
      \expr{\QEX{y}{\AOR{y=\MTS}{y=\{\MTS\}}}{ x\in y}}
      \expl{\equiv}{syntactic sugar for existential quantification; propositional logic}
      \expr{\QEXS{y}{\AOR{\AND{y =\MTS}{x\in y}}{\AND{y = \{\MTS\}}{ x\in y}}}}
      \expl{\equiv}{axiom of empty set $(Cx1)$: no element belongs in the empty set}
      \exprnnl{\QEXS{y}{\AOR{\SFALSE}{\AND{y = \{\MTS\}}{ x\in y}}}}
    \end{align*}
    \begin{align*}
      \expl{\equiv}{syntactic sugar for existential quantification; propositional logic}
      \expr{\QEX{y}{y=\{\MTS\}}{x \in y}}
      \expl{\equiv}{exactly one element satisfies the range}
      \exprnnl{x\in \{\MTS\}.}
    \end{align*}
  }
%
Thus, $\bigcup \{\MTS, \{\MTS\}\}=\{\MTS\}$. \qed
\end{example}
Another illustrative example is reasoning with function
composition. If $f$ and $g$ are two functions, then $g\circ f$ is
defined by:
%
\[\QALLS{x,y}{\AIFF{(x,y)\in g\circ f}{\QEXS{z}{\AAND{(x,z)\in f}{(z,y) \in g}}}}.\]
%
As in~\cite{hrbacek-intro-1999}, the expression $\left\langle f_i\mid
i\in I \right\rangle$ denotes the function $f$ with domain $I$.  For
example, the function $f(x)=x^2$ with domain $[0,1]$ can be
represented as $\left\langle x^2\mid x\in [0,1] \right\rangle$. The
formula $(u,v) \in \left\langle x^2\mid x\in [0,1] \right\rangle$
means that $v=u^2$ and that $u\in [0,1]$. An example
in~\cite{hrbacek-intro-1999} is to calculate $\left\langle
\sqrt{x}\mid x>0 \right\rangle\circ \left\langle x^2+1\mid x\in
\mathbb R \right\rangle $. For that purpose, the authors use an extra
theorem to first determine the domain of the composition and then
proceed to compute it. As presented by Example~\ref{exa.proofs.funcs},
the preliminary theorem is not required because the domain of the
composition can be obtained simultaneously with the proof.
%
\begin{example}\label{exa.proofs.funcs}
  The goal is to compute $\left\langle \sqrt{x}\mid x>0 \right\rangle\circ
  \left\langle x^2-1\mid x\in \mathbb R \right\rangle$:
{\small\begin{align*}
\expr{(u,v)\in \left\langle \sqrt{x}\mid x>0 \right\rangle\circ \left\langle 
x^2-1\mid x\in \mathbb R \right\rangle}
\expl{\equiv}{definition of function composition}
\expr{\QEXS{z}{\AAND{(u,z)\in \left\langle x^2-1 \mid x\in \mathbb R 
      \right\rangle}{(z,v)\in \left\langle \sqrt{x}\mid x>0 \right\rangle}}}
\expl{\equiv}{syntactic sugar ; $\left\langle \_ \mid \_\right\rangle$ 
notation }
\expr{\QEXS{z}{\AAND{z= u^2-1}{\AAND{u\in \mathbb R}{v= \sqrt{z}\wedge z>0}}}}
\expl{\equiv}{Axiom $(Bx3)$; Axiom $(Bx1)$}
\expr{\AAND{u\in \mathbb R}{\QEXS{z}{\AAND{z= u^2-1}{\AAND{v= \sqrt{z}}{z>0}}}}}
\expl{\equiv}{only one element satisfies the range}
\expr{\AAND{u\in \mathbb R}{\AAND{v= \sqrt{u^2-1}}{u^2-1>0 }}}
  %% \end{align*}
  %% \begin{align*}
\expl{\equiv}{syntactic sugar ; $\left\langle \_ \mid \_\right\rangle$ 
notation }
\exprnnl{(u,v)\in \left\langle \sqrt{x^2-1}\mid x^2-1>0 \right\rangle.}
\end{align*}}
%
Therefore, $\left\langle \sqrt{x}\mid x>0 \right\rangle\circ
\left\langle x^2-1\mid x\in \mathbb R \right\rangle=\left\langle
\sqrt{x^2-1}\mid x^2>1 \right\rangle$. \qed
\end{example}
%
% \begin{example}\label{exa.proofs.inv}
%   The goal is to compute the inverse of $\left\langle
%   \dfrac{1}{x^2}\mid x \neq 0 \right\rangle$:
% %
% \begin{align*}
% \expr{(u,v)\in \left\langle \dfrac{1}{x^2}\mid x \neq 0 \right\rangle^{-1}}
% \expl{\equiv}{definition of inverse relation}
% \expr{(v,u)\in \left\langle \dfrac{1}{x^2}\mid x \neq 0\right\rangle}
% \expl{\equiv}{syntactic sugar: $\langle \_\mid\_ \rangle$ notation}
% \expr{u=\dfrac{1}{v^2}\wedge v \neq 0 }
% \expl{\equiv}{arithmetic}
% \expr{\left(v=\dfrac{1}{\sqrt{u}} \vee  v=\dfrac{-1}{\sqrt{u}}\right)  \wedge u > 0}
% \expl{\equiv}{propositional algebra}
% \expr{\left( v=\dfrac{1}{\sqrt{u}} \wedge u>0  \right) \vee \left( v=\dfrac{-1}{\sqrt{u}}\wedge u>0\right)}
% \expl{\equiv}{syntactic sugar: $\langle \_\mid\_ \rangle$ notation}
% \expr{(u,v)\in \left\langle \dfrac{1}{\sqrt{x}} \mid x > 0 \right\rangle \vee (u,v)\in \left\langle \dfrac{-1}{\sqrt{x}} \mid x > 0 \right\rangle}
% \expl{\equiv}{definition of union}
% \exprnnl{(u,v)\in \left\langle \dfrac{1}{\sqrt{x}} \mid x > 0 \right\rangle \cup \left\langle \dfrac{-1}{\sqrt{x}} \mid x > 0 \right\rangle}
% \end{align*}
% %
% Then, $\left\langle \dfrac{1}{x^2}\mid x \neq 0 \right\rangle^{-1}=
% \left\langle \dfrac{1}{\sqrt{x}} \mid x > 0 \right\rangle \cup
% \left\langle \dfrac{-1}{\sqrt{x}} \mid x > 0 \right\rangle$. Note that
% the resulting set is not a function.
% \end{example}

\subsection{Discovery of Logical Structure}

The calculative style requires writing the propositions in a very
precise language that ultimately reveals their logical structure. This
makes it possible to carry out the required transformations from one
proposition to another in a proof more transparently than when using a
language that has not been designed for such a purpose.  In an
axiomatic theory, arguments in a proof are expected to be very
precise, leaving aside -- as far as possible --, colloquial ones. For
example, it can be shown that the Cartesian product of two sets is
empty iff one of its factors is empty. Of course, nobody doubts this
fact but, in order to proceed formally, it requires a proof.

\begin{example}\label{exa.proofs.cart}
  Prove $\vdash_\SETT \QALLS{x,y}{\AIFF{x \times y = \MTS}{\OR{x=\MTS}{y=\MTS}}}$.
{\small\begin{align*}
\expr{x\times y=\MTS}
\expl{\SIFF}{axiom of empty set $(Cx1)$}
\expr{\QALLS{u,v}{\ANEG{(u,v)\in x\times y}}}
\expl{\SIFF}{axiom of Cartesian product $(Cx14)$}
\expr{\QALLS{u,v}{\ANEG{\AND{u\in x}{v \in y}}}}
\expl{\SIFF}{ propositional logic: De Morgan's law}
\expr{\QALLS{u,v}{\AOR{u\notin x}{v \notin y}}}
\expl{\SIFF}{ first-order logic }
\expr{\AOR{\QALLS{u}{u \notin x}}{\QALLS{v}{v \notin y}}}
\expl{\SIFF}{axiom of empty set $(Cx1)$}
\exprnnl{\AOR{x=\MTS}{y=\MTS}.}
\end{align*}}
Therefore, $\QALLS{x,y}{\AIFF{x \times y =
    \MTS}{\OR{x=\MTS}{y=\MTS}}}$ is a theorem of $\SETT$.  \qed
\end{example}

One of the objectives of teaching a set theory course is to develop
the ability to properly write all arguments of a proof in natural
language. This task is usually a complex one for students, especially
when the arguments are related with Boolean reasoning. This is because
such a reasoning is used implicitly in proofs, leaving the bitter
feeling that the argument is correct but without clarifying the
reasons. In argumentative proofs, in general, the reasoning rests more
on aesthetic matters rather than on logical ones. Within the $\SETT$
formal system all arguments are made explicit, which helps to improve
the clarity and forcefulness of the proofs without resorting to the
elegance and skill in the use of the natural language.

In Example~\ref{exa.proofs.funcs2}, it is proved that a function is
invertible iff it is one to one. This is a case of use of appropriate
language when searching for a proof. The predicate $\text{fun}(f)$
stands for ``$f$ is a function'', $\text{inv} (f)$ for ``$f$ is
invertible'', and $\text{oto}(f)$ for ``$f$ is one to
one''. Symbolically,
%
\begin{description}
\item[] $\QALLS{f}{\AIFF{\text{fun} (f)}{\QALL{x,y,z}{\AAND{(x,y)\in f}{(x,z)\in f}}{y=z}}}$.
\item[] $\QALLS{f}{\AIFF{\text{inv} (f)}{\text{fun} (f^{-1})}}$.
\item[] $\QALLS{f}{\AIFF{\text{oto} (f)}{\QALL{x,y,z}{\AAND{(x,z)\in f}{(y,z)\in f}}{x=y}}}$.
\end{description}

\begin{example}\label{exa.proofs.funcs2}
  Prove $\vdash_\SETT \QALLS{x}{\AIFF{\text{inv}(f)}{\text{oto}(f)}}$.
%
{\small\begin{align*}
\expr{\text{inv} (f)}
\expl{\SIFF}{definition of invertible function}
\expr{\text{fun}( f^{-1}) }
\expl{\SIFF}{definition of function}
\exprnnl{\QALL{x,y,z}{\AAND{(x,y)\in f^{-1}}{(x,z)\in f^{-1}}}{y=z}}
  \end{align*}
  \begin{align*}
\expl{\SIFF}{definition of inverse relation}
\expr{\QALL{x,y,z}{\AAND{(y,x)\in f}{(z,x)\in f}}{y=z}}
\expl{\SIFF}{definition of one to one function }
\exprnnl{\text{oto} (f).}
\end{align*}}\qed
\end{example}

Example~\ref{exa.proofs.wo} presents a proof in $\SETT$ that natural
numbers with the usual order $<$ are a well-ordered set. The set
$\mathbb N$ of natural numbers is the smallest inductive set and
membership is a strictly linear relation. Well-ordering of $\nats$
means that all non-empty subsets of $\nats$ have a first (or
$<$-minimal) element. Consider the set of least numbers $x_\text{min}$
of a given set $x \subseteq \nats$:
%
\[\QALL{x}{x \subseteq \nats}{x_\text{min} = \SET{y \in x}{\QALL{z}{z \in x}{z \leq y}}}.\]
%
Of course, if $x_{\text{min}}$ is not empty, then it is unitary. Next
consider the unary predicate $\text{wo}_{<}$ defined as follows:
%
\[\QALL{x}{x\subseteq \nats}{\AIFF{\text{wo}_{<}(x)}{\QALL{y}{y \subseteq x}{\AIMP{y \neq \MTS}{y_\text{min}\neq \MTS}}}}.\]
%
Note that $\text{wo}_<(\nats)$ means that the set of natural numbers
is well-ordered. The definition of well-order can equivalently be
written, thanks to $\vdash_\DSL
\AIFF{\IMP{\varphi}{\psi}}{\IMP{\ANEG{\psi}}{\ANEG{\varphi}}}$, as:
%
\[\QALL{x}{x\subseteq \nats}{\AIFF{\text{wo}_{<}(x)}{\QALL{y}{y \subseteq x}{\AIMP{y_\text{min} = \MTS}{y = \MTS}}}}.\]
%
In addition, Example~\ref{exa.proofs.wo} uses a form of derivation in
which logical implication is allowed to relate a deduction step. Such
a sequence is called \textit{relaxed derivation} and the reader is
referred to~\cite{rocha-dsmeseguer-2015} for its definition and
properties.
%
\begin{example}\label{exa.proofs.wo}
  Prove $\vdash_\SETT \QALL{x}{x\subseteq \nats}{\AIMP{x_\text{min} = \MTS}{x = \MTS}}$.
{\small\begin{align*}
\expr{x_{\text{min}}=\MTS}
\expl{\SIFF}{axiom of empty set $(Cx1)$}
\expr{\QALLS{n}{n \notin x_\text{min}}}
\expl{\SIFF}{definition of $x_{\text{min}}$}
\expr{\QALLS{n}{\AOR{n\notin x}{\QEX{k}{k \in x}{k < n}}}}
\expl{\SIFF}{first-order logic}
\expr{\QALLS{n}{\AIMP{\QALL{k}{k < n}{k\notin x}}{n\notin x}}}
\expl{\SIMP}{induction principle for natural numbers}
\expr{\QALLS{n}{n \notin x}}
\expl{\SIFF}{axiom of empty set $(Cx1)$}
\exprnnl{x=\MTS.}
\end{align*}}\qed
\end{example}

\subsection{Proof Structure and Organization}

The calculative style can be used to anticipate auxiliary lemmas.
Indeed, this is the case in some induction theorems, such as the proof
of commutativity of natural number addition. Addition of natural
numbers is as a function $\func{+}{\nats \times \nats}{\nats}$ defined
à la Peano by:
\begin{enumerate}
\item[] $\QALL{m}{m \in\nats}{+(0,m) = m}$, 
\item[] $\QALL{m,n}{\AAND{m\in\nats}{n\in\nats}}{+(m,+(n,1)) = +(+(m,n),1)}$.
\end{enumerate}
The goal is to prove that natural number addition is commutative, i.e,
that
\[\QALL{m,n}{\AAND{m\in\nats}{n\in\nats}}{+(m,n) = +(n,m)}.\]
%
In~\cite[Thm.4.4, p.53]{hrbacek-intro-1999}), the proof of this fact
is hard to follow because there is an auxiliary induction proof inside
the main induction proof:
\begin{quotation}
	{\small \noindent We prove that every $n\in \nats$ commutes, by
    induction on $n$. To show that $0$ commutes, it suffices to show
    that $0+m=m$ for all $m$. .... Clearly $0+0=0$, and if $0+m=m$,
    then $0+(m+1)=(0+m)+1=m+1$. So the claim follows by induction (on
    $m$). Let us assume that $n$ commutes, an let us show that $n+1$
    commutes. We prove, by induction on $m$, that $m+(n+1)=(n+1)+m$
    for all $n\in \nats$. ...(proof follows)... }
\end{quotation}
%
In~\cite[Sec.13, p.50]{halmos-naive-1974}, the following is the proof
of the same property:
\begin{quotation}
{\small \noindent The proof that addition is commutative ... is a
  little tricky; a straightforward attack might fail. The trick is to
  prove, by induction on $n$, that (i) $0+n=n$ and (ii)
  $m^{+}+n=(m+n)^{+}$ and then to prove the desire commutativity
  equation by induction on $m$, via (i) y (ii). }
\end{quotation}
It will be better for a student to identify beforehand some lemmas
needed for the proof.

\begin{example}
  Note that the theorem statement has the form
  $\QALL{n}{n\in\nats}{q(n)}$,
  where \[\AIFF{q(n)}{\QALL{m}{m \in \nats}{p(m,n)}}.\] Some
  lemmas can be found from this formulation:
%
  {\small \begin{align*}
      \expr{\QALL{n}{n\in\nats}{q(n)}}
      \expl{\SCON}{induction principle}
      \exprnnl{\AAND{q(0)}{\QALL{n}{q(n)}{q(n+1)}}.}
\end{align*}}
%
\noindent Therefore, the goal now is to first prove $\QALL{m}{m \in
  \nats}{p(m,0)}$ and then, under hypothesis $\QALL{m}{m \in
  \nats}{p(m,n)}$, prove $\QALL{m}{m\in\nats}{p(m,n+1)}$.
\end{example}

%% \begin{example}
%%   Note that the theorem statement has the form
%%   $\QALL{n}{n\in\nats}{p(\nats,n)}$,
%%   where \[\AIFF{p(\nats,n)}{\QALL{m}{m \in \nats}{p(m,n)}}.\] Some
%%   lemmas can be found from this formulation:
%% %
%%   {\small \begin{align*}
%%       \expr{\QALL{n}{n\in\nats}{p(\nats,n)}}
%%       \expl{\equiv}{induction principle}
%%       \exprnnl{\AAND{p(\nats,0)}{\QALL{n}{p(\nats,n)}{p(\nats,n+1)}}.}
%% \end{align*}}
%% %
%% \noindent Therefore, the goal now is to first prove $\QALL{m}{m \in
%%   \nats}{p(m,0)}$ and then, under hypothesis $p(\mathbb N, n)$, prove
%% $\QALL{m}{m\in\nats}{p(m,n+1)}$.
%% \end{example}
