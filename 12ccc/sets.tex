\section{An Axiomatic Set Theory}
\label{sec.sets}

This section presents $\SETT$, a Zermelo-Franekel first-order system
in the language $\LSET = (\vars,\funcs,\preds)$, that results from
extending $\DSLS$ with axioms for sets. In $\LSET$, the infinitely
many variables $\vars$ range over elements in the domain of discourse,
$\funcs$ contains the constant $\MTS$ and the unary symbols
${\bigcup},{\POWER}$, and the only predicate symbol in $\preds$ is the
binary symbol ${\in}$. Intuitively, the function symbols represent the
empty set, generalized union, and the power set; the predicate symbol
${\in}$ represents membership.  The axioms of $\SETT$ in
Definition~\ref{def.sets.axioms} include axiomatic definitions for all
symbols in $\funcs$, meaning that $\in$ is a complete connective for
$\SETT$ (i.e., the entire language of set theory can be built from the
membership predicate symbol). Symbols not in $\LSET$ such as the
binary function symbols ${\cup}$ and $\cap$ denoting union and
intersection, respectively, and the binary predicate symbol
$\subseteq$ denoting inclusion can be added by means of the usual
axiomatic definitions. Some examples will be given at the end of the
section.

Note that in $\LSET$ there is no mention of the common `curly braces'
notation $\{\_\mid\_\}$ used for identifying collections, because this
notation can also be seen as an abbreviation just like $\emptyset$ or
$\subseteq$. Technically, $\{\_\mid\_\}$ is a binary meta-symbol used
as a term-forming operator that can be defined with the
\textit{definite description} operator $\iota$, for any variable $x
\in \vars$ and formula $\varphi \in \Fforms{\vars}{\funcs}{\preds}$,
as follows:
%
\[\AIFF{\SET{x}{\varphi(x)}}{\left(\iota y \mid : \QALLS{x}{\AIFF{x\in y}{\varphi(x)}}\right)}.\]
%
It can be shown, although it is beyond the scope of this paper, that
$\SET{x}{\varphi}$ identifies a unique element. Therefore, $\SETT$
allows the `curly braces' notation to be used as an abbreviation for a
unique element in the domain of discourse. The reader is referred
to~\cite[p.~126]{tourlakis-sets-2003} for details on the definite
description operator and its properties.

The notion of univalent formula is needed before introducing the
axioms of $\SETT$. A formula
$\varphi\in\Fforms{\vars}{\funcs}{\preds}$ is \textit{univalent} iff
$\QALL{x,y,z}{\AAND{\varphi(x,z)}{\varphi(y,z)}}{x = y}$. Intuitively,
if $\varphi$ is univalent and $\varphi(x,z)$ is true, then $x$ is the
only element that makes $\varphi(\_\,,z)$ true.

\begin{definition}\label{def.sets.axioms}
  Let $\varphi,\psi\in\Fforms{\vars}{\funcs}{\preds}$ be such that
  $\varphi$ has one free variable and $\psi$ is univalent.  The axioms
  of $\SETT$ are given by the following axiom schemata:
  %
  \begin{description}
  \item[$(Cx1)$] $\QALLS{x}{\AIFF{x = \MTS}{\QALLS{y}{\ANEG{y \in x}}}}$.
  \item[$(Cx2)$] $\QALLS{x,y}{\AIFF{x = y}{\QALLS{u}{\AIFF{u \in x}{u \in y}}}}$.
  \item[$(Cx3)$] $\QALLS{x,y,z}{\AIFF{x = \{y,z\}}{\QALLS{u}{\IFF{u \in
          x}{\AOR{u=y}{u=z}}}}}$.
  \item[$(Cx4)$] $\QALLS{x,y}{\AIFF{y = \{u \in x \mid
      \varphi(u)\}}{\QALLS{u}{\AIFF{u \in y}{\AAND{u\in
            x}{\varphi(u)}}}}}$.
  \item[$(Cx5)$] $\QALLS{x,y}{\AIFF{y=\bigcup x}{\QALLS{u}{\AIFF{u \in
          y}{\QEX{z}{z \in x}{u \in z}}}}}$.
  \item[$(Cx6)$] $\QALLS{x,y}{\AIFF{y=\POWER x}{\QALLS{u}{\AIFF{u \in
          y}{\QALL{z}{z\in u}{z \in x}}}}}$.
  \item[$(Cx7)$] $\QALLS{x,y}{\AIFF{y = \psi[x]}{\QALLS{u}{\AIFF{u\in
          y}{\QEX{z}{z \in x}{\psi(u,z)}}}}}$.
  \item[$(Cx8)$] $\QEX{x}{\MTS \in x}{\QALL{y}{y \in x}{y \cup \{y\}
      \in x}}$.
  \end{description}
  %
\end{definition}

The \textit{axiom of existence} $(Cx1)$ serves two purposes: first, it
states the existence of an unique set without elements, namely, the
empty set; second, it is a `definitional extension' for the function
symbol $\MTS$, which is the name assigned to the empty set. Note that,
by identifying `the' set without elements with $\MTS$, there is the
need to prove that such a set is unique (this proof is left to the
reader as a routine exercise after covering this section). The
\textit{axiom of extensionality} $(Cx2)$ characterizes equality: two
elements are equal whenever they have the same elements. The
\textit{axiom of pairing} $(Cx3)$ states the existence of an element
having two given elements. The \textit{axiom schema of separation}
$(Cx4)$, which represents as many axioms as formulas $\varphi$ with
exactly one variable are, states how an element can be obtained from
other element by selecting exactly those elements that satisfy a given
formula. The \textit{axiom of union} $(Cx5)$ and the \textit{axiom of
  power} $(Cx6)$, respectively, define generalized union and the power
element construction. The \textit{axiom schema of replacement} $(Cx7)$
uses an univalent formula to define an element $\psi[x]$ comprising
precisely those elements witnessing the satisfaction of $\psi(\_,z)$,
for each $z \in x$.  Finally, the \textit{axiom of infinity} $(Cx8)$
introduces the existence of (at least) one \textit{inductive set}: (i)
$\MTS$ belongs to this set; and (ii) if $x$ belongs to this set, then
$x\cup\{x\}$ (i.e., its \textit{successor set}) is also one of its
members. It is easy to see that such sets must necessarily have
infinitely many elements starting from $\MTS$, the successor of
$\MTS$, and so on. Also note that in Definition~\ref{def.sets.axioms}
the only axiom schemata are $(Cx4)$ and $(Cx7)$ because they are
parametric on given formulas.

In general, these axioms are similar to the ones usually studied in
graduate-level axiomatic set theory courses. A contribution of $\SETT$
is a rewrite of the axioms in the notation of
Dijkstra-Scholten. However, as illustrated in
Section~\ref{sec.proofs}, the main contribution of $\SETT$ is that it
enables an undergraduate proof-based course on set theory using simple
algebraic manipulation.

One important cornerstone of any axiomatic set theory, including
$\SETT$, is the distinction between elements that are `well-behaved'
and those that are not. More precisely, axiomatic set theory
distinguishes the elements that can be called a \textit{set} from
others that are not, namely, the broader concept of a
\textit{class}. Technically, a class is any collection, but a set is a
more refined version of a class: a set is a collection that can be
\textit{identified} by only using the axioms in
Definition~\ref{def.sets.axioms}. For instance, $\MTS$ and $\{\MTS\}$
are sets because of axioms $(Cx1)$ and
$(Cx8)$. Theorem~\ref{thm.sets.nouniv} presents a fundamental theorem
of $\SETT$, with a proof à la Dijkstra-Scholten, and identifies a
class that is not a set: the collection of all sets.

\begin{theorem}\label{thm.sets.nouniv}
  There exists no universal set.
\end{theorem}

\begin{proof}
  Towards a contradiction, assume such a set $V$ exists. Thus,
  $\vdash_{\SETT} \QALLS{x}{x \in V}$. Consider the set $S = \SET{x
    \in V}{x \notin x}$, in which $x \notin x$ abbreviates $\ANEG{x
    \in x}$:

  {\small\begin{align*}
    \expr{S \in S}
    \expl{\SIFF}{definition of $S$}
    \expr{S \in \SET{x \in  V}{x \notin x}}
    \expl{\SIFF}{ axiom of separation $(Cx4)$ } 
    \expr{S \in V \land  S \notin S}
    \expl{\SIFF}{$V$ is a universal set} 
    \expr{\AAND{\STRUE}{S \notin S}} 
    \expl{\SIFF}{ propositional logic } 
    \exprnnl{S \notin S.}
  \end{align*}}
  That is, $\vdash_\SETT \AIFF{S \in S}{S \notin S}$, which is a
  contradiction. Therefore, $V$ cannot exist. \qed
\end{proof}

As mentioned at the beginning of this section, other usual function
and predicate symbols can be added to $\SETT$ by means of definitional
extensions. Some of these symbols are included in
Definition~\ref{def.sets.ext}.

\begin{definition}\label{def.sets.ext}
  The following axioms define pairing, binary union and intersection,
  difference, and inclusion:
  \begin{description}
    \item[$(Cx10)$] $\QALLS{x,y,z}{\AIFF{x = (y,z)}{\QALLS{u}{\AIFF{u \in x}{\AOR{u = \{y\}}{u = \{y,z\}}}}}}$.
    \item[$(Cx11)$] $\QALLS{x,y,z}{\AIFF{x = y \cup z}{\QALLS{u}{\AIFF{u \in x}{\AOR{u \in y}{u \in z}}}}}$.
    \item[$(Cx12)$] $\QALLS{x,y,z}{\AIFF{x = y \cap z}{\QALLS{u}{\AIFF{u \in x}{\AAND{u \in y}{u \in z}}}}}$.
    \item[$(Cx13)$] $\QALLS{x,y,z}{\AIFF{x = y \setminus z}{\QALLS{u}{\AIFF{u \in x}{\AAND{u \in y}{u \notin z}}}}}$.
    \item[$(Cx14)$] $\QALLS{x,y,z}{\AIFF{x = y \times z}{\QALLS{u}{\AIFF{u \in x}{\QEX{v,w}{\AAND{v \in y}{w \in z}}{u = (v,w)}}}}}$.
    \item[$(Cx15)$] $\QALLS{x,y}{\AIFF{x \subseteq y}{\QALL{u}{u \in x}{u \in y}}}$.
  \end{description}
\end{definition}

Other operations such as the generalized Cartesian product and
generalized intersections, and the axiom of choice can be defined
similarly in the syntax of $\SETT$.
